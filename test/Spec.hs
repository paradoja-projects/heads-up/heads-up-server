{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
module Main
  ( main
  )
where

import           Database.PostgreSQL.Simple    as PG
import           HeadsUp.Api                    ( app )
import           Test.Hspec
import           Test.Hspec.Wai

main :: IO ()
main = hspec spec

-- todo: test cors
-- todo: fix tests :(

connectionString :: BS.ByteString
connectionString = "dbname=heads_up_test"

spec :: Spec
spec = with (return $ app conn) $ do
  describe "GET /game/room-0/" $ do
    it "responds with 200" $ do
      get "/game/room-0" `shouldRespondWith` 200

    it "responds with [UserAssignment]" $ do
      let
        users
          = "[{\"name\":\"Isaac\",\"character\":\"ghana pallbearer number 1\"},{\"name\":\"Albert\",\"character\":\"ghana pallbearer number 2\"}]"
      get "/game/room-0" `shouldRespondWith` users
