{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# OPTIONS_GHC -fno-warn-orphans  #-}
module HeadsUp
  ( startApp
  , app
  )
where

import           Data.ByteString.Char8          ( pack )
import           Database.PostgreSQL.Simple    as PG
import           Network.Wai.Handler.Warp       ( run )
import           Network.Wai.Middleware.Cors
import           Network.Wai.Middleware.Servant.Options
import           Network.Wai.Middleware.RequestLogger
                                                ( logStdoutDev )
import           Servant
import           Servant.Foreign.Internal
import           System.Environment             ( getEnv )
import           HeadsUp.Api


instance (HasForeign lang ftype api) => HasForeign lang ftype (BasicAuth a b :> api) where
  type Foreign ftype (BasicAuth a b :> api) = Foreign ftype api
  foreignFor lang proxy1 Proxy = foreignFor lang proxy1 (Proxy :: Proxy api)

startApp :: IO ()
startApp = do
  port <- read <$> getEnv "PORT"
  conn <- getEnv "DATABASE_URL" >>= PG.connectPostgreSQL . pack
  run port
    $ cors (const $ Just policy)
    $ provideOptions api
    $ logStdoutDev
    $ app conn
 where
  policy = simpleCorsResourcePolicy
    { corsRequestHeaders = ["content-type", "authorization"]
    }
