{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module HeadsUp.Models.RoomInfo where

import           Data.Text                      ( Text )
import           Control.Lens
import           Data.Aeson
import           GHC.Generics
import           HeadsUp.Models.UserAssignment
import           HeadsUp.Models.Room
import           HeadsUp.Models.Internal        ( jsonNaming )
import           HeadsUp.Models.User

data RoomInfo = RoomInfo
  { _roomInfoRoomName :: Text
  , _roomInfoStatus :: RoomStatus
  , _roomInfoAdmin :: [Text]
  , _roomInfoOtherPlayers :: [UserAssignment] }
  deriving (Eq, Show, Generic)
$(makeLenses ''RoomInfo)

lengthRoomInfoPrefix :: Int
lengthRoomInfoPrefix = length ("_roomInfo" :: String)

instance ToJSON RoomInfo where
  toJSON = genericToJSON defaultOptions
    { fieldLabelModifier = jsonNaming lengthRoomInfoPrefix
    }

instance FromJSON RoomInfo where
  parseJSON = genericParseJSON defaultOptions
    { fieldLabelModifier = jsonNaming lengthRoomInfoPrefix
    }

mkRoomInfo :: Room -> [User] -> RoomInfo
mkRoomInfo r uu = RoomInfo (_roomName r) ReadyToPlay admins userAssignments
 where
  admins          = (^. userName) <$> filter (^. userAdmin) uu
  userAssignments = fmap fromUser uu
