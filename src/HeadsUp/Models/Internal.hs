module HeadsUp.Models.Internal where

import           Data.Char                      ( toLower )

lowerFirst :: String -> String
lowerFirst (fc : cc) = toLower fc : cc
lowerFirst cc        = cc

jsonNaming :: Int -> String -> String
jsonNaming dropLength = lowerFirst . drop dropLength
