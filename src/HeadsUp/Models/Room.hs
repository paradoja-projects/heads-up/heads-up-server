{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module HeadsUp.Models.Room where

import           Control.Lens
import           Data.Aeson
import           Data.Text                      ( Text
                                                , unpack
                                                )
import           Database.Beam
import           Database.Beam.Backend.SQL
import           Database.Beam.Postgres


data RoomStatus
  = RoomPregameSetup | ReadyToPlay
  deriving (Eq, Show, Read, Generic, ToJSON, FromJSON)

instance HasSqlValueSyntax be String => HasSqlValueSyntax be RoomStatus where
  sqlValueSyntax = autoSqlValueSyntax
instance FromBackendRow Postgres RoomStatus where
  fromBackendRow = read . unpack <$> fromBackendRow

data RoomT f
    = Room
    { _roomName :: C f Text
    , _status   :: C f RoomStatus}
    deriving (Generic, Beamable)
$(makeLenses ''RoomT)

type Room = RoomT Identity
deriving instance Show Room
deriving instance Eq Room
instance ToJSON Room
instance FromJSON Room

type RoomId = PrimaryKey RoomT Identity
instance Table RoomT where
  data PrimaryKey RoomT f = RoomId (Columnar f Text)
                          deriving (Generic, Beamable)
  primaryKey = RoomId . _roomName

deriving instance Show RoomId
deriving instance Eq RoomId
instance ToJSON RoomId
instance FromJSON RoomId
