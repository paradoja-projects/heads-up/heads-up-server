{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module HeadsUp.Models.UserAssignment where

import           Data.Text                      ( Text )
import           Control.Lens
import           Data.Aeson
import           GHC.Generics
import           HeadsUp.Models.User
import           HeadsUp.Models.Internal        ( jsonNaming )

data UserAssignment = UserAssignment
  { _userAssignmentName      :: Text
  , _userAssignmentCharacter :: Text }
  deriving (Show, Eq, Generic)
$(makeLenses ''UserAssignment)

lengthUserAssignmentPrefix :: Int
lengthUserAssignmentPrefix = length ("_userAssignment" :: String)

instance ToJSON UserAssignment where
  toJSON = genericToJSON defaultOptions
    { fieldLabelModifier = jsonNaming lengthUserAssignmentPrefix
    }

instance FromJSON UserAssignment where
  parseJSON = genericParseJSON defaultOptions
    { fieldLabelModifier = jsonNaming lengthUserAssignmentPrefix
    }


fromUser :: User -> UserAssignment
fromUser (User name _ _ _) = UserAssignment name "ghana pallbearer number 1"
