{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE StandaloneDeriving #-}
module HeadsUp.Models.User where

import           Control.Lens
import           Data.Aeson
import           Data.Text                      ( Text )
import           Database.Beam
import           HeadsUp.Models.Room
import           HeadsUp.Models.Internal        ( jsonNaming )

data UserT f
    = User
    { _userName       :: C f Text
    , _userSimplePass :: C f Text
    , _userAdmin      :: C f Bool

    , _userInRoom     :: PrimaryKey RoomT f }
    deriving (Generic, Beamable)
$(makeLenses ''UserT)

type User = UserT Identity
deriving instance Show User
deriving instance Eq User
instance ToJSON User
instance FromJSON User

type UserId = PrimaryKey UserT Identity
instance Table UserT where
  data PrimaryKey UserT f = UserId (Columnar f Text) (PrimaryKey RoomT f)
                          deriving (Generic, Beamable)
  primaryKey = UserId <$> _userName <*> _userInRoom

data SimpleUser
  = SimpleUser
  { simpleUserUserName   :: Text
  , simpleUserSimplePass :: Text
  }
  deriving (Generic, Show, Eq)

lengthSimpleUserPrefix :: Int
lengthSimpleUserPrefix = length ("simpleUser" :: String)

instance ToJSON SimpleUser where
  toJSON = genericToJSON defaultOptions
    { fieldLabelModifier = jsonNaming lengthSimpleUserPrefix
    }

instance FromJSON SimpleUser where
  parseJSON = genericParseJSON defaultOptions
    { fieldLabelModifier = jsonNaming lengthSimpleUserPrefix
    }

toSimpleUser :: User -> SimpleUser
toSimpleUser u = SimpleUser (_userName u) (_userSimplePass u)
