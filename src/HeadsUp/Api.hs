{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
{-# LANGUAGE FlexibleContexts #-}
module HeadsUp.Api
  ( API
  , api
  , app
  )
where

import           Control.Lens
import           Data.List                      ( find )
import           Data.Text                      ( Text )
import           Data.Text.Encoding             ( decodeUtf8 )
import           Data.Tuple                     ( swap )
import           Database.Beam
import           Network.Wai
import           Servant
import           HeadsUp.Database
import           HeadsUp.Models

type API = "game" :> Capture "room" Text :> BasicAuth "game" SimpleUser :> Get '[JSON] RoomInfo
      :<|> "game" :> Capture "room" Text :> "player" :> ReqBody '[JSON] SimpleUser :> Post '[JSON] RoomInfo
      :<|> "game" :> Capture "room" Text :> ReqBody '[JSON] SimpleUser :> Post '[JSON] RoomInfo

api :: Proxy API
api = Proxy

server :: Connection -> Server API
server conn = getRoomInfoH conn :<|> newPlayerH conn :<|> createRoomH conn

app :: Connection -> Application
app conn = serveWithContext api ctx $ server conn
  where ctx = getSimpleUser :. EmptyContext

getSimpleUser :: BasicAuthCheck SimpleUser
getSimpleUser = BasicAuthCheck $ \basicAuthData ->
  let uName = decodeUtf8 (basicAuthUsername basicAuthData)
      sPass = decodeUtf8 (basicAuthPassword basicAuthData)
  in return $ Authorized $ SimpleUser uName sPass

getUsersForRoom :: RoomId -> DbSelect UserT s
getUsersForRoom rName =
  filter_ ((val_ rName ==.) . _userInRoom) $ all_ (headsUpDb ^. users)

createRoomH :: Connection -> Text -> SimpleUser -> Handler RoomInfo
createRoomH conn rName su = do
  let r = Room rName RoomPregameSetup
  ri <- liftIO $ catchViolation violationCatcher $ runTransaction conn $ do
    runInsert $ insert (headsUpDb ^. rooms) $ insertValues [r]
    let name = simpleUserUserName su
    let u    = User name (simpleUserSimplePass su) True (pk r)
    runInsert $ insert (headsUpDb ^. users) $ insertValues [u]

    return $ Right $ RoomInfo rName ReadyToPlay [name] []
  either throwError return ri
  where violationCatcher _ (UniqueViolation _) = return $ Left err400
        violationCatcher _ _ = return $ Left err500

getRoomInfoT :: MonadDBQuery m => Text -> m (Maybe (Room, [User]))
getRoomInfoT rName = do
  maybeRoom <- runSelectReturningOne
               $ lookup_ (headsUpDb ^. rooms) (RoomId rName)
  maybeUsers <- mapM
                (runSelectReturningList . select . getUsersForRoom . pk)
                maybeRoom
  return $ bisequence (maybeRoom, maybeUsers)
 where
  bisequence :: Monad m => (m a, m b) -> m (a, b)
  bisequence = ((sequence . swap) =<<) . sequence . swap

useRoomInfoT :: Connection -> Text -> ((Room, [User]) -> MonadDB a) -> (a -> Handler b) -> Handler b
useRoomInfoT conn rName dbF returnF = do
  mResult <- liftIO $ runTransaction conn $ do
    maybeRoomAndUsers <- getRoomInfoT rName
    case maybeRoomAndUsers of
      Nothing -> return Nothing
      Just (r, us) -> do
        a <- dbF (r, us)
        return $ Just a
  maybe (throwError err404) returnF mResult

newPlayerH :: Connection -> Text -> SimpleUser -> Handler RoomInfo
newPlayerH conn rName su =
  useRoomInfoT conn rName
  (\(r, us) -> liftIO $ runTransaction conn $ do
    let name = simpleUserUserName su
    let u    = User name (simpleUserSimplePass su) False (pk r)
    runInsert $ insert (headsUpDb ^. users) $ insertValues [u]
    return (r, us ++ [u])) $
  \(r, us) ->
    let userAuthenticated = maybe False
                            ((== su) . toSimpleUser)
                            (find ((simpleUserUserName su ==) . _userName) us )
    in if userAuthenticated
       then return $ mkRoomInfo r us
       else throwError err401

getRoomInfoH :: Connection -> Text -> SimpleUser -> Handler RoomInfo
getRoomInfoH conn rName su =
  useRoomInfoT conn rName return $
  \(r, us) ->
    let userAuthenticated = maybe False
                            ((== su) . toSimpleUser)
                            (find ((simpleUserUserName su ==) . _userName) us)
    in if userAuthenticated
       then return $ mkRoomInfo r us
       else throwError err401
