{-# LANGUAGE TemplateHaskell  #-}
{-# LANGUAGE DeriveGeneric    #-}
{-# LANGUAGE DeriveAnyClass   #-}
{-# LANGUAGE ConstraintKinds  #-}
{-# LANGUAGE FlexibleContexts #-}
module HeadsUp.Database
  ( module HeadsUp.Database
  , module Database.PostgreSQL.Simple.Errors
  , Database.Beam.Postgres.Connection
  )
where

import           Control.Monad                  ( liftM2 )
import           Control.Lens
import           Database.Beam
import           Database.Beam.Postgres
import           Database.PostgreSQL.Simple     ( withTransaction )
import           Database.PostgreSQL.Simple.Errors
                                                ( catchViolation
                                                , ConstraintViolation(..)
                                                )
import           HeadsUp.Models


type MonadDB = Pg
type MonadDBQuery = MonadBeam Postgres
type DbSelect t s = Q Postgres HeadsUpDb s (t (QExpr Postgres s))

data HeadsUpDb f = HeadsUpDb
                   { _rooms :: f (TableEntity RoomT)
                   , _users :: f (TableEntity UserT) }
                 deriving (Generic, Database be)
$(makeLenses ''HeadsUpDb)

headsUpDb :: DatabaseSettings be HeadsUpDb
headsUpDb = defaultDbSettings

runTransaction :: Connection -> Pg a -> IO a
runTransaction = liftM2 (.) withTransaction runBeamPostgres

runDb :: Connection -> Pg a -> IO a
runDb = runBeamPostgres
