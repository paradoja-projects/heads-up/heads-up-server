drop table users;
drop table rooms;



create table rooms
(name varchar not null,
 status varchar not null,
 primary key(name));

create table users
(name varchar not null,
 simple_pass varchar not null,
 admin boolean not null,

 in_room__name varchar not null references rooms(name),
 primary key (name, in_room__name));



insert into "rooms" (name, status) values ('room-0', 'ReadyToPlay');

insert into "users" (name, simple_pass, admin, in_room__name)
             values ('albert', 'einstein', false, 'room-0');
insert into "users" (name, simple_pass, admin, in_room__name)
             values ('isaac', 'newton', true, 'room-0');
